/*
Теоретичні питання
1.Опишіть, як можна створити новий HTML тег на сторінці.
Новий HTML тег на сторінці можна створити методом document.createElement(tag)
2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Перший параметр функції insertAdjacentHTML - це спеціальне слово, що вказує куди по відношенню до елемента робити вставку.
Ось можливі варінти:
- "beforebegin" - вставить html текст безпосередньо перед елементом;
- "afterbegin" - вставить html текст на початку елемента;
- "beforeend" - вставить html текст в кінці елемента;
- "afterend" - вставить html текст безпосередньо після елементу.
3.Як можна видалити елемент зі сторінки?
Щоб видалити елемент зі сторінки існує метод node.remove. Тобто спочатку потрібно обрати і записати елемент який видаляється,
а потім астосувати метод remove()
    */

function displayList(array, parent = document.body) {
    const listContainer = document.createElement("ul");

    array.forEach(item => {
        const listItem = document.createElement("li");
        listItem.textContent = item;
        listContainer.appendChild(listItem);
    });

    parent.appendChild(listContainer);
}

const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
displayList(array);